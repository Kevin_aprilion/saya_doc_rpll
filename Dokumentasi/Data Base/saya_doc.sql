-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2020 at 09:24 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saya_doc`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartobat`
--

CREATE TABLE `cartobat` (
  `idData` int(10) DEFAULT NULL,
  `idObat` int(10) DEFAULT NULL,
  `namaObat` varchar(100) DEFAULT NULL,
  `quantity` int(2) DEFAULT NULL,
  `harga` int(8) DEFAULT NULL,
  `total` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `datapasien`
--

CREATE TABLE `datapasien` (
  `idData` int(10) NOT NULL,
  `idPasien` int(10) DEFAULT NULL,
  `idKontributor` int(10) DEFAULT NULL,
  `namaPasien` varchar(15) DEFAULT NULL,
  `jadwal` date DEFAULT NULL,
  `sakit` varchar(100) DEFAULT NULL,
  `tindakan` text DEFAULT NULL,
  `bayarDokter` int(10) DEFAULT NULL,
  `keluhan` varchar(255) DEFAULT NULL,
  `totalBayar` int(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `bayarObat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kontributor`
--

CREATE TABLE `kontributor` (
  `idKontributor` int(10) NOT NULL,
  `namaKontributor` varchar(100) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `idPasien` int(10) NOT NULL,
  `namaPasien` varchar(100) DEFAULT NULL,
  `noTelepon` int(15) DEFAULT NULL,
  `alamat` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `stockobat`
--

CREATE TABLE `stockobat` (
  `idObat` int(10) NOT NULL,
  `namaObat` varchar(100) DEFAULT NULL,
  `quantity` int(2) DEFAULT NULL,
  `harga` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartobat`
--
ALTER TABLE `cartobat`
  ADD KEY `idData` (`idData`,`idObat`),
  ADD KEY `idObat` (`idObat`);

--
-- Indexes for table `datapasien`
--
ALTER TABLE `datapasien`
  ADD PRIMARY KEY (`idData`),
  ADD KEY `idPasien` (`idPasien`),
  ADD KEY `idKontributor` (`idKontributor`);

--
-- Indexes for table `kontributor`
--
ALTER TABLE `kontributor`
  ADD PRIMARY KEY (`idKontributor`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`idPasien`);

--
-- Indexes for table `stockobat`
--
ALTER TABLE `stockobat`
  ADD PRIMARY KEY (`idObat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `datapasien`
--
ALTER TABLE `datapasien`
  MODIFY `idData` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kontributor`
--
ALTER TABLE `kontributor`
  MODIFY `idKontributor` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `idPasien` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stockobat`
--
ALTER TABLE `stockobat`
  MODIFY `idObat` int(10) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartobat`
--
ALTER TABLE `cartobat`
  ADD CONSTRAINT `cartobat_ibfk_1` FOREIGN KEY (`idObat`) REFERENCES `stockobat` (`idObat`),
  ADD CONSTRAINT `cartobat_ibfk_2` FOREIGN KEY (`idData`) REFERENCES `datapasien` (`idData`);

--
-- Constraints for table `datapasien`
--
ALTER TABLE `datapasien`
  ADD CONSTRAINT `datapasien_ibfk_1` FOREIGN KEY (`idKontributor`) REFERENCES `kontributor` (`idKontributor`),
  ADD CONSTRAINT `datapasien_ibfk_2` FOREIGN KEY (`idPasien`) REFERENCES `pasien` (`idPasien`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
