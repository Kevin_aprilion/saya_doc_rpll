-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2020 at 05:32 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saya_doc`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartobat`
--

CREATE TABLE `cartobat` (
  `idData` int(10) NOT NULL,
  `idObat` int(10) NOT NULL,
  `namaObat` varchar(100) NOT NULL,
  `quantity` int(2) NOT NULL,
  `harga` int(8) NOT NULL,
  `total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cartobat`
--

INSERT INTO `cartobat` (`idData`, `idObat`, `namaObat`, `quantity`, `harga`, `total`) VALUES
(1, 1, 'Diatabs', 100, 2500, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 1, 'Diatabs', 100, 2500, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 1, 'Diatabs', 100, 2500, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 1, 'Diatabs', 100, 2500, 0),
(1, 1, 'Diatabs', 100, 2500, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 1, 'Diatabs', 100, 2500, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 1, 'Diatabs', 100, 2500, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 2, 'Panadol Biru', 100, 10000, 0),
(1, 1, 'Diatabs', 100, 1, 0),
(1, 1, 'Diatabs', 0, 2500, 0),
(1, 2, 'Panadol Biru', 0, 10000, 0),
(1, 1, 'Diatabs', 1, 2500, 0),
(1, 1, 'Diatabs', 1, 2500, 0),
(1, 1, 'Diatabs', 1, 2500, 0),
(1, 1, 'Diatabs', 1, 2500, 0),
(1, 2, 'Panadol Biru', 1, 10000, 0),
(5, 2, 'Panadol Biru', 1, 10000, 0),
(6, 1, 'Diatabs', 1, 2500, 0),
(7, 2, 'Panadol Biru', 1, 10000, 0),
(8, 1, 'Diatabs', 1, 2500, 0),
(8, 2, 'Panadol Biru', 1, 10000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `datapasien`
--

CREATE TABLE `datapasien` (
  `idData` int(10) NOT NULL,
  `idPasien` int(10) NOT NULL,
  `idKontributor` int(10) NOT NULL,
  `namaPasien` varchar(15) NOT NULL,
  `jadwal` date NOT NULL,
  `sakit` varchar(100) NOT NULL,
  `tindakan` text NOT NULL,
  `bayarDokter` int(10) NOT NULL,
  `keluhan` varchar(30) NOT NULL,
  `status` varchar(10) NOT NULL,
  `bayarObat` int(11) NOT NULL,
  `totalBiaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `datapasien`
--

INSERT INTO `datapasien` (`idData`, `idPasien`, `idKontributor`, `namaPasien`, `jadwal`, `sakit`, `tindakan`, `bayarDokter`, `keluhan`, `status`, `bayarObat`, `totalBiaya`) VALUES
(1, 7, 1, 'asd', '2020-04-20', 'diare', 'obat', 85000, 'sakit perut', 'lunas', 140001, 225001),
(2, 8, 1, 'kevin', '2020-04-20', '', 'belum', 0, 'sakit kepala', 'belum', 0, 0),
(4, 10, 1, '', '2020-04-21', '', 'belum', 0, 'kepala nyeri', 'belum', 0, 0),
(5, 10, 1, 'seto', '2020-04-21', 'demam', 'obat', 80000, 'meriang', 'lunas', 10000, 90000),
(6, 11, 1, 'sudin', '2020-04-21', 'diare', 'obat', 80000, 'keram perut', 'lunas', 2500, 82500),
(7, 12, 1, 'chester', '2020-04-21', 'demam', 'obat', 80000, 'kurang enak badan', 'lunas', 10000, 90000),
(8, 13, 1, 'rahmat', '2020-04-22', 'diare', 'obat', 80000, 'keram perut', 'lunas', 12500, 92500);

-- --------------------------------------------------------

--
-- Table structure for table `kontributor`
--

CREATE TABLE `kontributor` (
  `IdKontributor` int(10) NOT NULL,
  `namaKontributor` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kontributor`
--

INSERT INTO `kontributor` (`IdKontributor`, `namaKontributor`, `password`, `pekerjaan`, `keterangan`) VALUES
(1, 'dokter', 'admin', 'dokter', 'umum'),
(3, 'resep', 'admin', 'resepsionis', 'resepsionis'),
(4, 'kasir', 'admin', 'kasir', 'kasir'),
(5, 'farmasi', 'admin', 'farmasi', 'farmasi');

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `idPasien` int(10) NOT NULL,
  `namaPasien` varchar(100) NOT NULL,
  `noTelepon` int(15) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`idPasien`, `namaPasien`, `noTelepon`, `alamat`) VALUES
(1, '', 0, ''),
(2, '', 0, ''),
(3, '', 0, 'Buuah batu'),
(4, '123', 0, ''),
(5, '', 0, '123'),
(6, '', 0, 'Buuah batu'),
(7, 'asd', 8771234, 'asd'),
(8, 'Kevin', 8771234, '123'),
(9, 'budiman', 98987, 'ujung berung'),
(10, 'seto', 12354, 'cihampelas'),
(11, 'sudin', 8771234, 'kopo'),
(12, 'chester', 13146123, 'amerika'),
(13, 'rahmat', 78565575, 'ujung berung');

-- --------------------------------------------------------

--
-- Table structure for table `stockobat`
--

CREATE TABLE `stockobat` (
  `idObat` int(10) NOT NULL,
  `namaObat` varchar(100) NOT NULL,
  `quantity` int(2) NOT NULL,
  `harga` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stockobat`
--

INSERT INTO `stockobat` (`idObat`, `namaObat`, `quantity`, `harga`) VALUES
(1, 'Diatabs', 100, 2500),
(2, 'Panadol Biru', 100, 10000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartobat`
--
ALTER TABLE `cartobat`
  ADD KEY `idData` (`idData`,`idObat`),
  ADD KEY `idObat` (`idObat`);

--
-- Indexes for table `datapasien`
--
ALTER TABLE `datapasien`
  ADD PRIMARY KEY (`idData`),
  ADD KEY `idPasien` (`idPasien`,`idKontributor`),
  ADD KEY `idRole` (`idKontributor`);

--
-- Indexes for table `kontributor`
--
ALTER TABLE `kontributor`
  ADD PRIMARY KEY (`IdKontributor`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`idPasien`);

--
-- Indexes for table `stockobat`
--
ALTER TABLE `stockobat`
  ADD PRIMARY KEY (`idObat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `datapasien`
--
ALTER TABLE `datapasien`
  MODIFY `idData` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kontributor`
--
ALTER TABLE `kontributor`
  MODIFY `IdKontributor` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `idPasien` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `stockobat`
--
ALTER TABLE `stockobat`
  MODIFY `idObat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartobat`
--
ALTER TABLE `cartobat`
  ADD CONSTRAINT `cartobat_ibfk_1` FOREIGN KEY (`idObat`) REFERENCES `stockobat` (`idObat`),
  ADD CONSTRAINT `cartobat_ibfk_2` FOREIGN KEY (`idData`) REFERENCES `datapasien` (`idData`);

--
-- Constraints for table `datapasien`
--
ALTER TABLE `datapasien`
  ADD CONSTRAINT `datapasien_ibfk_1` FOREIGN KEY (`idKontributor`) REFERENCES `kontributor` (`IdKontributor`),
  ADD CONSTRAINT `datapasien_ibfk_2` FOREIGN KEY (`idPasien`) REFERENCES `pasien` (`idPasien`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
