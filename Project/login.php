<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <title>Home</title>
      <meta name="author" content="hanna-budi-kevin-juan">
      <meta charset="utf-8">
      <link rel="stylesheet" href="css/main.css">
      <link rel="stylesheet" href="css/learn.css">
      <link rel="stylesheet" href="css/jquery-ui.css">
      <script src="assets/javascript/jquery-3.2.1.min.js"></script>
      <script src="assets/javascript/jquery-ui.js"></script>

      <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,700" rel="stylesheet">
      <script>
          $(document).ready(function(){
              $(".tablinks.child").css("display","none");
              $(".collapse").click(function(){
                  $(".tablinks.child").toggle(200);
              });
          });
      </script>
</head>
<body>
  <div class="page">
    <!--Banner and headers-->
    <div class="navbar">
      <div class="dropdown">
        <article class="innerb"><h1>Home</h1></article>
      </div>
    </div>
    <div class="cbanner">
      <article class="innerb"><h1>Login</h1></article>
    </div>
  </div>
  <div class="content1">
      <form action="api/login_session.php" method="POST">
        <fieldset>
        <legend>Login Contributor</legend>
        <p>
            <label>Username:</label>
            <input type="text" name="Username" placeholder="username..." />
        </p>
        <p>
            <label>Password:</label>
            <input type="password" name="Password" placeholder="password..." />
        </p>
        <p>
            <label><input type="checkbox" name="remember" value="remember" /> Remember me</label>
        </p>
        <p>
            <input type="submit" name="submit" value="Login" />
        </p>
        </fieldset>
    </form>

  </div>


</body>
</html>
