<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Fitur Resepsionis</title>
    <meta name="author" content="hanna-budi-kevin-juan">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/learn.css">
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css">
    <script src="assets/javascript/jquery-3.2.1.min.js"></script>
    <script src="assets/javascript/jquery-ui.js"></script>
  </head>
  <body>
    <div class="page">
      <!--Banner and headers-->
      <div class="navbar">
        <a href="index.html" class="selected">HOME</a>
        <a href="resepsionis.html">RESEPSIONIS</a>
        <a href="doctor.html">DOCTOR</a>
        <a href="farmasi.html">FARMASI</a>
        <a href="kasir.html">KASIR</a>
      </div>
      <div class="cbanner">
        <article class="innerb"><h1>Registrasi</h1></article>
      </div>
    </div>
    <div class="footer">
      <div id="content3h" style="">
        <h2>Masukan Data Pasien</h2>
      </div>
      <div class="inner3">
        <div class="suppbox">
          <form action="ProsesRegis.php" method="POST">
            <p>Nama Pasien</p><input type="text" name= "nama" required><br>
            <p>Alamat</p><input type="text" name= "alamat" required><br>
            <p>No Telp</p><input type="text" name="Notelp" required><br>
            <button type="submit"">Daftar</button>
          </form>
        </div>
      </div>
    </div>
    <div class="footer2">
        <footer>Copyright &copy; SAYA DOK by Hanna Kevin Budi Juan.</footer>
    </div>
  </body>
</html>
