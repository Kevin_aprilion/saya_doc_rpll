<!DOCTYPE html>
<?php
session_start();
  $myfile = fopen('active-thread.txt','w');
        fwrite($myfile,$_GET['id']);
        fclose($myfile);
?>
<html lang="en" dir="ltr">
  <head>
    <title>Fitur Doctor</title>
    <meta name="author" content="hanna-budi-kevin-juan">
    <meta charset="utf-8">
    <link rel="stylesheet" href="../../css/main.css">
    <link rel="stylesheet" href="../../css/learn.css">
    <link rel="stylesheet" href="../../css/jquery-ui.css">
    <script src="assets/javascript/jquery-3.2.1.min.js"></script>
    <script src="assets/javascript/jquery-ui.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,700" rel="stylesheet">
    <script>
      $(document).ready(function(){
        $(".tablinks.child").css("display","none");
        $(".collapse").click(function(){
          $(".tablinks.child").toggle(200);
        });
      });
    </script>
  </head>
  <body>
    <div class="page">
      <!--Banner and headers-->
      <div class="navbar">
        <a href="#" class="selected">Doctor</a>
      </div>
      <div class="cbanner">
        <article class="innerb"><h1>Doctor</h1></article>
      </div>
    </div>
    <div class="sidebar">
      <h3>Navigations</h3>
      <a href="#" class="selected" id="nav">Check Appointment</a>
      <a href="cari_riwayat_pasien.php" id="nav">Mencari Riwayat Pasien</a>
      <a href="rekam_medis.php" id="nav">Lihat Rekam Medis</a>
      <a href="../../logout.php" id="nav" id="nav">Log Out</a>
    </div>
    <div class="content1">
      <form action="../../api/hasil_pemeriksaan.php" method="POST">
        <fieldset>
          <legend>Hasil Pemeriksaan</legend>
          <div class="content1L">
            <table class="center">
              <tr>
                <td>Sakit</td>
                <td>:</td>
                <td><input type="text" name="sakit" placeholder="Sakit" /></td>
              </tr>
              <tr>
                <td>Tindakan</td>
                <td>:</td>
                <td><input type="text" name="tindakan" placeholder="Tindakan" /></td>
              </tr>
              <tr>
                <td>Biaya</td>
                <td>:</td>
                <td><input type="number" name="biaya" placeholder="Biaya" /></td>
              </tr>
            </table>
          </div>
          <input type="submit" name="submit" value="submit" />
        </fieldset>
      </form>
    </div>
  </body>
</html>
