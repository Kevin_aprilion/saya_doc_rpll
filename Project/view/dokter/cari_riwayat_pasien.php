<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en" dir="ltr">
<head>
  <title>Fitur Doctor</title>
      <meta name="author" content="hanna-budi-kevin-juan">
      <meta charset="utf-8">
      <link rel="stylesheet" href="../../css/main.css">
      <link rel="stylesheet" href="../../css/learn.css">
      <link rel="stylesheet" href="../../css/jquery-ui.css">
      <script src="assets/javascript/jquery-3.2.1.min.js"></script>
      <script src="assets/javascript/jquery-ui.js"></script>
      <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,700" rel="stylesheet">
      <script>
          $(document).ready(function(){
              $(".tablinks.child").css("display","none");
              $(".collapse").click(function(){
                  $(".tablinks.child").toggle(200);
              });
          });
      </script>
</head>
  <body>
    <div class="page">
            <!--Banner and headers-->
      <div class="navbar">
      <a href="#" class="selected">Doctor</a>
      </div>
      
      <div class="cbanner">
        <article class="innerb"><h1>Riwayat Pasien</h1></article>
      </div>
    </div>
    <div class="sidebar">
      <h3>Navigations</h3>
      <a href="../doctor.php" class="selected" id="nav">Check Appointment</a>
      <a href="cari_riwayat_pasien.php" id="nav">Mencari Riwayat Pasien</a>
      <a href="rekam_medis.php" id="nav">Lihat Rekam Medis</a>
      <a href="../../logout.php" id="nav" id="nav">Log Out</a>
    </div>
  
  <?php

    $id = $_SESSION['IdKontributor'];
    
    $connect = mysqli_connect("localhost", "root", "", "saya_doc");

    $arr_user = array();
    $result = mysqli_query($connect,"SELECT * FROM datapasien where IdKontributor = '$id' and tindakan != 'belum'");
    echo "<div class='content1'>";
      echo "<div id='content1h'>";
       echo "<h2>Yang Pernah Di Tanganin</h2>";
      echo "</div>";
      echo "<table id='compare'>";
        echo "<tr>";
          echo "<th>Nama Pasien</th>";
          echo "<th>Jadwal</th>";
          echo "<th>Sakit Pernah di derita</th>";
        echo "</tr>";
        
    while($row = mysqli_fetch_array($result)){
      echo "<tr>";
              echo "<td>" . $row[3] . "</td>";
              echo "<td>" . $row[4] . "</td>";
              echo "<td>" . $row[5] . "</td>";
       echo "</tr>";       
      }
      
      echo "</table>";
    echo "</div>";
?>
</body>
</html>
