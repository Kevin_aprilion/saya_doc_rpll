<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en" dir="ltr">
  <head>
    <title>Lihat Riwayat Medis</title>
    <meta name="author" content="hanna-budi-kevin-juan">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/learn.css">
    <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,700" rel="stylesheet">
    <script src="assets/javascript/jquery-3.2.1.min.js"></script>
    <script src="assets/javascript/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            $(".tablinks.child").css("display","none");
            $(".collapse").click(function(){
                $(".tablinks.child").toggle(200);
            });
        });
    </script>
  </head>
  <body>
    <div class="page">
      <!--Banner and headers-->
      <div class="navbar">
        <a href="../resepsionis.php" class="selected">Resepsionis</a>
      </div>
      </div>
      <div class="cbanner">
        <article class="innerb"><h1>Resepsionis - Riwayat Medis</h1></article>
      </div>
    </div>
    <div class="sidebar">
      <h3>Fitur Resepsionis</h3>
        <a href="../resepsionis.php" id="nav" id="nav">Bikin Appointment</a>
        <a href="registrasi.php" id="nav" id="nav">Registrasi</a>
        <a href="kunjungan.php" id="nav" id="nav">Riwayat Kunjungan</a>
        <a href="medis.php" id="nav" id="nav">Lihat Riwayat Medis</a>
        <a href="../../logout.php" id="nav" id="nav">Log Out</a>
    </div>
    <div class="content1">
      <form action="#riwayat" method="POST">
        <fieldset>
          <legend>Lihat Riwayat Medis</legend>
          <div class="content1L">
            <table class="center">
              <tr>
                <td>ID Pasien</td>
                <td>:</td>
                <td><input type="text" name="IDPasien" placeholder="Nama Pasien" /></td>
              </tr>
            </table>
            </div>
          <input type="submit" name="submit" value="submit" />
        </fieldset>
      </form>
    </div>
<?php
if(isset($_POST['submit'])){

    $IDPasien = $_POST['IDPasien'];
    
    $connect = mysqli_connect("localhost", "root", "", "saya_doc");

    $arr_user = array();
    $result = mysqli_query($connect,"SELECT * FROM datapasien where IdPasien = '$IDPasien'");
    echo "<div class='content1'>";
      echo "<div id='content1h'>";
       echo "<h2>Check Appointment</h2>";
      echo "</div>";
      echo "<table id='compare'>";
        echo "<tr>";
          echo "<th>Nama Pasien</th>";
          echo "<th>Sakit Pernah di derita</th>";
        echo "</tr>";
        echo "<tr>";
    while($row = mysqli_fetch_array($result)){
              echo "<td>" . $row[3] . "</td>";
              echo "<td>" . $row[5] . "</td>";
              
      }
      echo "</tr>";
      echo "</table>";
    echo "</div>";
  }
?>
  </body>
</html>
