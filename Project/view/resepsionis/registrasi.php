<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en" dir="ltr">
  <head>
    <title>Registrasi</title>
    <meta name="author" content="hanna-budi-kevin-juan">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/learn.css">
    <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,700" rel="stylesheet">
    <script src="assets/javascript/jquery-3.2.1.min.js"></script>
    <script src="assets/javascript/jquery-ui.js"></script>
    <script>
      $(document).ready(function(){
        $(".tablinks.child").css("display","none");
        $(".collapse").click(function(){
          $(".tablinks.child").toggle(200);
        });
      });
    </script>
  </head>
  <body>
    <div class="page">
      <!--Banner and headers-->
      <div class="navbar">
        <a href="../resepsionis.php" class="selected">Resepsionis</a>
      </div>
      </div>
      <div class="cbanner">
        <article class="innerb"><h1>Resepsionis - Registrasi</h1></article>
      </div>
    </div>
    <div class="sidebar">
      <h3>Fitur Resepsionis</h3>
      <a href="../resepsionis.php" id="nav" id="nav">Bikin Appointment</a>
      <a href="registrasi.php" id="nav" id="nav">Registrasi</a>
      <a href="kunjungan.php" id="nav" id="nav">Riwayat Kunjungan</a>
      <a href="medis.php" id="nav" id="nav">Lihat Riwayat Medis</a>
      <a href="../../logout.php" id="nav" id="nav">Log Out</a>
    </div>
    <div class="content1">
      <form action="../../api/insert_pasien.php" method="POST">
        <fieldset>
          <legend>Registrasi</legend>
          <div class="content1L">
            <table class="center">
              <tr>
                <td>Nama Pasien</td>
                <td>:</td>
                <td><input type="text" name="namaPasien" placeholder="Nama Pasien" /></td>
              </tr>
              <tr>
                <td>Alamat Pasien</td>
                <td>:</td>
                <td><input type="text" name="alamat" placeholder="alamat..." /></td>
              </tr>
              <tr>
                <td> No Tlp</td>
                <td>:</td>
                <td><input type="number" name="noTlp" placeholder="noTlp..." /></td>
              </tr>
            </table>
          </div>
          <input type="submit" name="submit" value="OK" />
        </fieldset>
      </form>
    </div>
  </body>
</html>
