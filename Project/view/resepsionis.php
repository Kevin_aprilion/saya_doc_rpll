<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en" dir="ltr">
  <head>
    <title>Fitur Resepsionis</title>
    <meta name="author" content="hanna-budi-kevin-juan">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/learn.css">
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,700" rel="stylesheet">
    <script src="assets/javascript/jquery-3.2.1.min.js"></script>
    <script src="assets/javascript/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            $(".tablinks.child").css("display","none");
            $(".collapse").click(function(){
                $(".tablinks.child").toggle(200);
            });
        });
    </script>
  </head>
  <body>
    <div class="page">
      <!--Banner and headers-->
      <div class="navbar">
        <a href="#" class="selected">Resepsionis</a>
      </div>
      </div>
      <div class="cbanner">
        <article class="innerb"><h1>Resepsionis - Appointment</h1></article>
      </div>
    </div>
    <div class="sidebar">
      <h3>Fitur Resepsionis</h3>
        <a href="#" id="nav" id="nav">Bikin Appointment</a>
        <a href="resepsionis/registrasi.php" id="nav" id="nav">Registrasi</a>
        <a href="resepsionis/kunjungan.php" id="nav" id="nav">Riwayat Kunjungan</a>
        <a href="resepsionis/medis.php" id="nav" id="nav">Lihat Riwayat Medis</a>
        <a href="../logout.php" id="nav" id="nav">Log Out</a>
    </div>
    <div class="content1">
       <form action="../api/insert_dataPasien.php" method="POST">
        <fieldset>
        <legend>Appointment</legend>
          <div class="content1L">
            <table class="center">
              <tr>
                <td>ID Pasien</td>
                <td>:</td>
                <td><input type="text" name="IdPasien" placeholder="Pasien" /></td>
              </tr>
              <tr>
                <td>Nama Pasien</td>
                <td>:</td>
                <td><input type="text" name="namaPasien" placeholder="Pasien" /></td>
              </tr>
              <tr>
                <td>ID Doctor</td>
                <td>:</td>
                <td><input type="text" name="IdDoctor" placeholder="Doctor" /></td>
              </tr>
              <tr>
                <td>Date</td>
                <td>:</td>
                <td><input type="date" id="date" name="date"></td>
              </tr>
              <tr>
                <td>Keluhan</td>
                <td>:</td>
                <td><textarea cols='20%' rows="15%"  name="keluhan" placeholder="Isi Keluhan" required></textarea></td>
              </tr>
            </table>
          </div>
          <input type="submit" name="submit" value="Appointment" />
        </fieldset>
      </form>
    </div>
  </body>
</html>
