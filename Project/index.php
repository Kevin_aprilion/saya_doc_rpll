<!DOCTYPE html>
<html>
    <head>
        <meta name="author" content="Budiman - Kevin - Hanna - Juan">
        <meta charset="utf-8">
        <link rel="icon" href="galery/pita.png">
        <link rel="stylesheet" href="css/main.css">
        <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,700" rel="stylesheet">
        <title>Saya Dok</title>
    </head>
    <body>
        <div class="page">
            <!--Banner and headers-->
            <div class="banner">
                <div class="navbar">
                    <a href="index.php" class="selected">HOME</a>
                    <a href="login.php" class="selected">Login</a>
                </div>
                <article class="innerbanner">
                    <h1>Saya Dok</h1>
                    <p>Come, Than you will feel better </p>
                </article>
            </div>
            <!--The contents-->
            <div class="content1">
                <div id="content1h">
                    <h2>WHAT IS A SAYA DOK?</h2>
                </div>
                <div class="inner1">
                    <div id="c11">
                        <img src="galery/konsultasiAnimasi1.png" alt="server">
                        <h3>Consultation</h3>
                        <p>Patients can consult a doctor about the disease or complaints</p>
                    </div>
                    <div id="c12">
                        <img src="galery/scedule.png" alt="SSD">
                        <h3>See Doctor's Schedule</h3>
                        <p>The server use more electricity to run its components. Also, more Memory and have different HDD and CPU than a normal computer, like we said, the server need to run for a long time, without stop.</p>
                    </div>
                    <div id="c13">
                        <img src="galery/doctorCheck.jpeg" alt="tools">
                        <h3>Examined by a Doctor</h3>
                        <p>after consultation and scheduling, then you will be examined by a doctor</p>
                    </div>
                </div>

            <div class="footer2">
                <footer>Copyright &copy; SAYA DOK by Hanna Kevin Budi Juan.</footer>
            </div>
        </div>
    </body>
</html>
