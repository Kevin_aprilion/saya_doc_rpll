<?php
require connection.php;

// api.php

class API {
	private $connect = '';

	function __construct() {
		$this->database_connection();
	}

	function database_connection() {
		$this->connect = new PDO("mysql:host=localhost;dbname=saya_doc", "root", "");
	}

    // insert data ke tabel pasien
	function insert() {
		if(isset($_POST["submit"])) {
			$form_data = array(
				':nama_pasien'  => $_POST["nama_pasien"],
				':alamat'  => $_POST["alamat"],
				':no_telepon'  => $_POST["no_telepon"]
			);
			$query = "
				INSERT INTO pasien 
				(namaPasien, alamat, noTelepon) VALUES 
				(:nama_pasien, :alamat, :no_telepon)
			";
			$statement = $this->connect->prepare($query);
			if($statement->execute($form_data)) {
				$data[] = array(
                'success' => '1'
                );
            } else {
                $data[] = array(
                    'success' => '0'
                );
            }
        } else {
            $data[] = array(
                'success' => '0'
            );
        }
    return $data;
    }

    // ambil semua data pasien
    function fetch_all() {
        $query = "SELECT * FROM pasien ORDER BY id_pasien";
        $statement = $this->connect->prepare($query);
        if($statement->execute()) {
            while($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $data[] = $row;
            }
        return $data;
        }
    }

    // ambil 1 data pasien
    function fetch_single($id_pasien) {
        $query = "SELECT * FROM pasien WHERE id_pasien='".$id_pasien."'";
        $statement = $this->connect->prepare($query);
        if($statement->execute()) {
            foreach($statement->fetchAll() as $row) {
                $data['nama_pasien'] = $row['nama_pasien'];
                $data['email'] = $row['email'];
            }
        return $data;
        }
    }

    // ambil histori 1 data pasien
    function fetch_single_history($id_pasien) {
        $query = "SELECT p.nama_pasien, h.keluhan FROM pasien p INNER JOIN histori_pasien h WHERE P.id_pasien = h.id_pasien AND id_pasien='" . $id_pasien . "'";
        $statement = $this->connect->prepare($query);
        if($statement->execute()) {
            foreach($statement->fetchAll() as $row) {
                $data['nama_pasien'] = $row['nama_pasien'];
                $data['email'] = $row['email'];
            }
        return $data;
        }
    }
}
?>