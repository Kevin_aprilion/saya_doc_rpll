<?php

    // connection.php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "saya_doc";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);

    // Check connection
    if (!$conn) {
        die("Connection failed: " . $conn);
    }
?>